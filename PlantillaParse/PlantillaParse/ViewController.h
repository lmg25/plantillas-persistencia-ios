//
//  ViewController.h
//  PlantillaParse
//
//  Created by Otto Colomina Pardo on 14/1/15.
//  Copyright (c) 2015 Universidad de Alicante. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *campoLogin;
@property (weak, nonatomic) IBOutlet UITextField *campoPassword;

@end

